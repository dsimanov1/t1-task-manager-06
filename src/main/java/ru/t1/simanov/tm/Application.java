package ru.t1.simanov.tm;

import ru.t1.simanov.tm.constant.ArgumentConst;
import ru.t1.simanov.tm.constant.TerminalConst;

import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }
        processCommands();
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK-MANAGER**");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showErrorArgument();
        }
    }

    private static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showErrorArgument() {
        System.err.println("Error! This argument not supported...");
    }

    public static void showErrorCommand() {
        System.err.println("Error! This command not supported...");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Dima Simanov");
        System.out.println("E-mail: simanov.dima@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show application version. \n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info. \n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show application commands. \n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application. \n", TerminalConst.EXIT);
    }

}
